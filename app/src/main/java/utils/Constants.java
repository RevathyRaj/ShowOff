package utils;

/**
 * Created by revathim on 08/07/2018.
 * constants to be used  globally
 */

public final class Constants {
    public static final String BASE_URL = "https://api.instagram.com/";
    public static final String CLIENT_ID = "63c30882cd77480088404c7d205f372e";
    public static final String REDIRECT_URI = "http://localhost:3000/";
    public static final String ACCESS_TOKEN = "access_token";
}
