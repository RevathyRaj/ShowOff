package utils;

import android.content.Context;
import android.content.SharedPreferences;

import utils.Constants;

/**
 * Created by revathim on 08/07/2018.
 * Saves strings in app memory for accessing offline
 */
public class AppPreferences {

    private SharedPreferences preferences;
    private Context ctx;

    public AppPreferences(Context prefscontext) {
        // super();
        this.ctx = prefscontext;
        preferences = prefscontext.getSharedPreferences("revathi.showoff.showoffapplication",
                Context.MODE_PRIVATE);
    }

    private SharedPreferences getSharedPrefs() {
        if (preferences == null) {
            preferences = ctx.getSharedPreferences("revathi.showoff.showoffapplication",
                    Context.MODE_PRIVATE);
        }
        return preferences;
    }

    /**
     * saves access token
     *
     * @param accessToken
     */
    public void saveAccessToken(String accessToken) {

        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(Constants.ACCESS_TOKEN, accessToken);
        editor.apply();
    }

    /**
     * retrieves access token
     */
    public String getAccessToken() {
        return preferences.getString(Constants.ACCESS_TOKEN, "");
    }

}
