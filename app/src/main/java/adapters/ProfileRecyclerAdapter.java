package adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit.retrofit.mediaresponses.Caption;
import retrofit.retrofit.mediaresponses.Datum;
import revathi.showoff.showoffapplication.R;

/**
 * Created by revathim on 08/07/2018.
 * Recycler adapter for posts screen
 */
public class ProfileRecyclerAdapter extends RecyclerView.Adapter<ProfileRecyclerAdapter.ViewHolder> {
    private List<Datum> postDataList;
    private Context context;

    // Provides a reference to the views for each data item
    // provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView likesTextView, commentsTextView, captionTextView;
        public ImageView postImageView;

        public ViewHolder(View v) {

            super(v);

            postImageView = v.findViewById(R.id.post_item_image_view);
            likesTextView = v.findViewById(R.id.likes_tv);
            commentsTextView = v.findViewById(R.id.comments_tv);
            captionTextView = v.findViewById(R.id.caption_tv);

        }
    }

    public ProfileRecyclerAdapter(Context context, List<Datum> data) {
        postDataList = data;
        this.context = context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ProfileRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_item_layout, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // get element from  postDataList at this position and replace the contents of the view with that element
        if (postDataList.get(position).getLikes() != null) {
            holder.likesTextView.setText("" + postDataList.get(position).getLikes().getCount());
        }
        if (postDataList.get(position).getComments() != null) {
            holder.commentsTextView.setText("" + postDataList.get(position).getComments().getCount());
        }
        if (postDataList.get(position).getCaption() != null) {
            Gson gson = new Gson();
            String json = gson.toJson(postDataList.get(position).getCaption());
            Caption caption = gson.fromJson(json, Caption.class);
            holder.captionTextView.setText("" + caption.getText());
        }
        if (postDataList.get(position).getImages() != null) {
            Picasso.with(context).load(postDataList.get(position).getImages()
                    .getStandardResolution().getUrl()).into(holder.postImageView);
        }


    }

    // Returns the size of  postDataList (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return postDataList.size();
    }
}