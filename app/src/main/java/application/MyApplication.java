package application;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;

import utils.AppPreferences;

/**
 * Created by revathim on 08/07/2018.
 * Loads when the app starts and stays in the memory for global access
 */
public class MyApplication extends Application {
    public static AppPreferences appPreferences;
    public static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        appPreferences = new AppPreferences(this);
        context = this;
    }

    public static boolean isNetworkAvailable() {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }
}
