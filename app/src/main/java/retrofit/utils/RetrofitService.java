package retrofit.utils;


import retrofit.responses.InstagramResponse;
import retrofit.retrofit.mediaresponses.RecentMediaresponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by revathim on 08/07/2018.
 * Calls instagram apis
 */

public interface RetrofitService {


    @GET("v1/users/self")
    Call<InstagramResponse> getProfile(@Query("access_token") String access_token);

    @GET("v1/users/self/media/recent")
    Call<RecentMediaresponse> getRecentPosts(@Query("access_token") String access_token);


}