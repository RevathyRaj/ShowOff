package activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import application.MyApplication;
import revathi.showoff.showoffapplication.R;
import utils.AppPreferences;

/**
 * Created by revathim on 08/07/2018.
 * Checks for access token and navigates user accordingly
 */
public class SplashActivity extends AppCompatActivity {
    private Button loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        loginButton = findViewById(R.id.sign_in_btn);
        loginButton.setVisibility(View.GONE);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                startActivity(new Intent(SplashActivity.this, AccessTokenActivity.class));
            }
        });

        /* New Handler to start the other Activity
         * and close this Splash-Screen after some seconds.*/
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (MyApplication.isNetworkAvailable()) {
                    AppPreferences appPreferences = new AppPreferences(SplashActivity.this);
                    Intent mainIntent;
                    if (appPreferences.getAccessToken() == null || appPreferences.getAccessToken().isEmpty()) {
                        loginButton.setVisibility(View.VISIBLE);

                    } else {

                        mainIntent = new Intent(SplashActivity.this, FeedActivity.class);
                        SplashActivity.this.startActivity(mainIntent);
                        SplashActivity.this.finish();
                    }
                } else {
                    Toast.makeText(SplashActivity.this,
                            getResources().getString(R.string.internet_error), Toast.LENGTH_LONG).show();
                }
            }
        }, 2000);
    }
}
