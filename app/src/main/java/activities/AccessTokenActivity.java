package activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import revathi.showoff.showoffapplication.R;
import utils.AppPreferences;
import utils.Constants;

/**
 * Created by revathim on 08/07/2018.
 * Fetches access token from instagram api
 */
public class AccessTokenActivity extends AppCompatActivity {
    private WebView web_view;
    private String url = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_access_token);

        url = Constants.BASE_URL
                + "oauth/authorize/?client_id="
                + Constants.CLIENT_ID
                + "&redirect_uri="
                + Constants.REDIRECT_URI
                + "&response_type=token"
                + "&display=touch&scope=public_content";
        initializeWebView();
    }

    /**
     * Initialize views
     */
    private void initializeWebView() {
        web_view = findViewById(R.id.web_view);
        web_view.loadUrl(url);
        web_view.setVerticalScrollBarEnabled(false);
        web_view.setHorizontalScrollBarEnabled(false);
        web_view.getSettings().setJavaScriptEnabled(true);
        web_view.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);

                if (url.contains("#access_token=")) {
                    Uri uri = Uri.parse(url);
                    String access_token = uri.getEncodedFragment();
                    // get the whole token after the '=' sign
                    access_token = access_token.substring(access_token.lastIndexOf("=") + 1);
                    AppPreferences appPreferences = new AppPreferences(AccessTokenActivity.this);
                    appPreferences.saveAccessToken(access_token);
                    finish();
                    startActivity(new Intent(AccessTokenActivity.this, FeedActivity.class));

                } else if (url.contains("?error")) {
                    Toast.makeText(AccessTokenActivity.this, "Error occurred", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
