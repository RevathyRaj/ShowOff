package activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import adapters.ProfileRecyclerAdapter;
import application.MyApplication;
import retrofit.responses.InstagramResponse;
import retrofit.retrofit.mediaresponses.RecentMediaresponse;
import retrofit.utils.RestClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import revathi.showoff.showoffapplication.R;
import utils.AppPreferences;

/**
 * Created by revathim on 08/07/2018.
 * Fetches data from instagram api and displays
 */
public class FeedActivity extends AppCompatActivity {

    private ImageView profileImageView;
    private TextView signOut;
    private TextView postsTextView, followersTextView, followingTextView;
    private RecyclerView postsRecyclerView;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private String access_token = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed);

        collapsingToolbarLayout = findViewById(R.id.collapse_toolbar);

        // Get the access_token from the intent extra
        AppPreferences appPreferences = new AppPreferences(this);
        access_token = appPreferences.getAccessToken();

        initializeViews();
        if (MyApplication.isNetworkAvailable()) {
            fetchProfile();
        } else {
            Toast.makeText(this, "Please connect to internet to proceed", Toast.LENGTH_LONG).show();
        }

    }

    /**
     * Instantiate views
     */
    private void initializeViews() {
        profileImageView = findViewById(R.id.profile_image_view);
        postsTextView = findViewById(R.id.posts_text_view);
        followersTextView = findViewById(R.id.followers_text_view);
        followingTextView = findViewById(R.id.following_text_view);
        postsRecyclerView = findViewById(R.id.recycler_view);
        signOut = findViewById(R.id.sign_out_tv);
        signOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppPreferences appPreferences = new AppPreferences(FeedActivity.this);
                appPreferences.saveAccessToken("");
                finish();
                startActivity(new Intent(FeedActivity.this, SplashActivity.class));
            }
        });
    }

    /**
     * Fetches profile information
     */
    private void fetchProfile() {
        Call<InstagramResponse> call = RestClient.getRetrofitService().getProfile(access_token);
        call.enqueue(new Callback<InstagramResponse>() {
            @Override
            public void onResponse(Call<InstagramResponse> call, Response<InstagramResponse> response) {
                if (response.body() != null) {
                    Picasso.with(FeedActivity.this).load(response.body().getData().getProfilePicture()).into(profileImageView);
                    collapsingToolbarLayout.setTitleEnabled(false);
                    getSupportActionBar().setTitle(response.body().getData().getUsername());
                    postsTextView.setText("" + response.body().getData().getCounts().getMedia());
                    followersTextView.setText("" + response.body().getData().getCounts().getFollowedBy());
                    followingTextView.setText("" + response.body().getData().getCounts().getFollows());
                    if (MyApplication.isNetworkAvailable()) {
                        fetchRecentMedia();
                    } else {
                        Toast.makeText(FeedActivity.this, getResources().getString(R.string.internet_error), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<InstagramResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * fetches recent media information
     */
    private void fetchRecentMedia() {
        Call<RecentMediaresponse> call = RestClient.getRetrofitService().getRecentPosts(access_token);
        call.enqueue(new Callback<RecentMediaresponse>() {
            @Override
            public void onResponse(Call<RecentMediaresponse> call, Response<RecentMediaresponse> response) {
                if (response.body() != null) {
                    ProfileRecyclerAdapter profileRecyclerAdapter =
                            new ProfileRecyclerAdapter(FeedActivity.this, response.body().getData());
                    postsRecyclerView.setAdapter(profileRecyclerAdapter);

                }
            }

            @Override
            public void onFailure(Call<RecentMediaresponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_LONG).show();
            }
        });
    }
}
