App loads by checking for access token. If it is present, then home screen is displayed with user’s recent media else login button is enabled.
Upon clicking login, webview with instagram’s login page is displayed, upon successful login user is redirected to home screen.
v Used Retrofit client library for api calls, as it makes the requests faster and handles asynchronous calls.
v Enabled proguard for obfuscation.
v Followed coding standards to organize all the classes in their respective packages. v Written all the strings in strings.xml file and no hard-coded strings are present.
v Used dimens.xml file to maintain screen designs consistent and support all screen
resolutions.
v Used icons from third party web sites.
v Used Application class to instantiate the globally used classes.
